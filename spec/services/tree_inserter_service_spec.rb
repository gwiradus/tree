require 'rails_helper'
require 'pry'

describe TreeInserterService do
    before :all do
        @one_level_tree = {"id" => 1, "child" => []}
        @two_level_tree = {"id" => 1, "child" => [{"id" => 2, "child" => []}, {"id" => 3, "child" => []}]}
        @three_level_tree = {"id" => 1, "child" => [{"id" => 2, "child" => [{"id" => 4, "child" => []}]}, {"id" => 3, "child" => []}]}
    end

    it 'should call methods correctly' do
        expect_any_instance_of(TreeInserterService).to receive(:process_tree).with(@one_level_tree, 1, "ROOT1").and_call_original

        expect_any_instance_of(TreeInserterService).to receive(:insert_j_tree).with(@one_level_tree, 1).times.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:build_node).once.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:process_children).with([], 1, 1, "ROOT1").and_call_original

        TreeInserterService.new(@one_level_tree).run
    end

    it 'should call methods correctly for a two-level tree' do
        expect_any_instance_of(TreeInserterService).to receive(:process_tree).with(@two_level_tree, 2, "ROOT2").and_call_original

        # child[1], child[2], child[3]
        expect_any_instance_of(TreeInserterService).to receive(:insert_j_tree).exactly(1).times.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:build_node).exactly(3).times.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:process_children).exactly(3).times.and_call_original
        TreeInserterService.new(@two_level_tree).run
    end

    it 'should call methods correctly for a 3-level tree' do
        expect_any_instance_of(TreeInserterService).to receive(:process_tree).with(@three_level_tree, 3, "ROOT3").and_call_original

        # child[1], child[2], child[3]
        expect_any_instance_of(TreeInserterService).to receive(:insert_j_tree).exactly(1).times.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:build_node).exactly(4).times.and_call_original
        expect_any_instance_of(TreeInserterService).to receive(:process_children).exactly(4).times.and_call_original
        TreeInserterService.new(@three_level_tree).run
    end
end