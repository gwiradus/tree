def process_tree(tree, path="TOP")
    build_node(tree, path)
end

def build_node(current_node, path)
    n = Node.create!(:node_id => current_node["id"], :path => path)
    puts n.inspect
    process_children(current_node["child"], current_node["id"], path)
end

def process_children(children, ancestor_id, path)
    path += ".#{ancestor_id}"
    children.each do |child|
        build_node(child, path)
    end
end