namespace :db do
    task :truncate => "db:load_config" do
        DatabaseCleaner.clean_with :truncation
    end
end