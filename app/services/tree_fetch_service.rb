require 'net/http'
require 'json'

class TreeFetchService < ActiveService::Base
    def initialize
    end

    def run
        uri = URI.parse('https://random-tree.herokuapp.com')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        req = Net::HTTP::Get.new("/")
        res = http.request(req)
        tree =  JSON.parse res.body
        TreeInserterService.new(tree).run
    end
end
