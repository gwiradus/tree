require 'json'
class TreeInserterService < ActiveService::Base
    def initialize(tree)
       @tree = tree
       @nodes = []
    end

    def run
        last_root_id = Node.roots.order(:node_id => :desc).first
        new_root_id = last_root_id.present? ? last_root_id.node_id + 1 : @tree["id"]
        @tree["id"] = new_root_id
        insert_j_tree(@tree, new_root_id)
        process_tree(@tree, new_root_id, "ROOT#{new_root_id}")
        Node.create!(@nodes)
    end

    def insert_j_tree(tree, tree_id)
        JTree.create!(:tree_id => tree_id, :tree => tree.to_json)
    end

    def process_tree(tree, tree_id, path)
        build_node(tree, tree_id, path)
    end
    
    def build_node(current_node, tree_id, path)
        n = {:node_id => current_node["id"], :tree_id => tree_id, :path => path}
        @nodes << n
        process_children(current_node["child"], current_node["id"], tree_id, path)
    end
    
    def process_children(children, ancestor_id, tree_id, path)
        path += ".#{ancestor_id}"
        children.each do |child|
            build_node(child, tree_id, path)
        end
    end
end