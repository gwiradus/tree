class Node < ApplicationRecord
    ltree :path

    def self.get_node(tree_id, node_id)
        find_by(:node_id => node_id, :tree_id => tree_id)
    end
end
