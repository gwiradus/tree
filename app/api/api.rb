class API < Grape::API
    format :json
    mount Tree::TreeAPI
    mount Tree::NodeAPI

    route :any, '*path' do
        error!("Route Not Found", 404)
    end
end