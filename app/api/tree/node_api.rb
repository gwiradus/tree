class Tree::NodeAPI < Grape::API
    helpers do
        def find_node(tree_id, node_id)
            @node = Node.get_node(params[:tree_id], params[:id])
        end

        def require_existing_node!
            error!("404 Node Not Found", 404) if @node.nil?
        end
    end

    route_param :tree_id, type: Integer do
        after_validation do
            find_node(params[:tree_id], params[:id])
            require_existing_node!
        end
        get 'parents/:id' do
            parent = @node.parent
            {:parent_id => parent.nil? ? nil : parent.node_id}
        end

        get 'child/:id' do
            child_ids = @node.children.pluck(:node_id)
            {:children => child_ids}
        end

        get 'ancestors/:id' do
            ancestors = @node.ancestors.pluck(:node_id)
            {:ancestors => ancestors}
        end

        get 'descendants/:id' do
            descendants = @node.descendants.pluck(:node_id)
            {:descendants => descendants}
        end
    end
end