require 'json'
class Tree::TreeAPI < Grape::API
    helpers do
        def find_tree(tree_id)
            @tree = JTree.find_by_tree_id(params[:tree_id])
        end

        def require_existing_tree!
            error!("404 Tree Not Found", 404) if @tree.nil?
        end
    end
    route_param :tree_id do
        after_validation do
            find_tree(params[:tree_id])
            require_existing_tree!
        end
        get do
            JSON.parse(@tree.tree).to_h
        end
    end

    get do
        tree_ids = JTree.pluck(:tree_id)
        {:available_trees => tree_ids}
    end
end