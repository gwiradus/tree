class CreateJTrees < ActiveRecord::Migration[5.2]
  def change
    create_table :j_trees do |t|
      t.integer :tree_id
      t.json :tree
    end
  end
end
