class CreateNodes < ActiveRecord::Migration[5.2]
  def change
    create_table :nodes do |t|
      t.column :node_id, :int
      t.column :tree_id, :int
      t.column :path, :ltree
    end

    add_index :nodes, :tree_id, :using => :btree
    add_index :nodes, :node_id, :using => :btree
    add_index :nodes, :path, :using => :gist
  end
end
