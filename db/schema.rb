# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_04_190008) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "ltree"
  enable_extension "plpgsql"

  create_table "j_trees", force: :cascade do |t|
    t.integer "tree_id"
    t.json "tree"
  end

  create_table "nodes", force: :cascade do |t|
    t.integer "node_id"
    t.integer "tree_id"
    t.ltree "path"
    t.index ["node_id"], name: "index_nodes_on_node_id"
    t.index ["path"], name: "index_nodes_on_path", using: :gist
    t.index ["tree_id"], name: "index_nodes_on_tree_id"
  end

end
